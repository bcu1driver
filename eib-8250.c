/*
     EIB 8250 serial port driver

     Copyright (C) 2000 Bernd Thallner <bernd@kangaroo.at>
     Copyright (C) 2005-2008 Martin Koegler <mkoegler@auto.tuwien.ac.at>

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/serial_reg.h>
#include <linux/interrupt.h>
#include <asm/io.h>

#include "eib-common.h"

typedef struct
{
  int port;
  int irq;
  long base;
  struct device dev;
} eib_config;

#define to_eib_config(d) container_of(d, eib_config, dev)

#define MAX_DEVICE_COUNT 4

/* private minor number use count */
static eib_config devs[MAX_DEVICE_COUNT] = {
  {.port = 0x3f8,.irq = 4,.base = 115200},
  {.port = 0x2f8,.irq = 3,.base = 115200},
  {.port = 0x3e8,.irq = 4,.base = 115200},
  {.port = 0x2e8,.irq = 3,.base = 115200},
};

static ssize_t
show_port (struct device *dev, struct device_attribute *attr, char *buf)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  return snprintf (buf, PAGE_SIZE, "%x\n", conf->port);
}

static ssize_t
store_port (struct device *dev, struct device_attribute *attr,
	    const char *buf, size_t count)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  conf->port = (int) simple_strtol (buf, NULL, 16);
  return count;
}

static ssize_t
show_irq (struct device *dev, struct device_attribute *attr, char *buf)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  return snprintf (buf, PAGE_SIZE, "%d\n", conf->irq);
}

static ssize_t
store_irq (struct device *dev, struct device_attribute *attr, const char *buf,
	   size_t count)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  conf->irq = (int) simple_strtol (buf, NULL, 10);
  return count;
}

static ssize_t
show_base (struct device *dev, struct device_attribute *attr, char *buf)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  return snprintf (buf, PAGE_SIZE, "%ld\n", conf->base);
}

static ssize_t
store_base (struct device *dev, struct device_attribute *attr,
	    const char *buf, size_t count)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  conf->base = (int) simple_strtol (buf, NULL, 10);
  if (conf->base <= 0)
    conf->base = 115200;

  return count;
}

static ssize_t
show_porttype (struct device *dev, struct device_attribute *attr, char *buf)
{
  eib_config *conf = to_eib_config (dev);
  if (conf == NULL)
    return 0;

  return snprintf (buf, PAGE_SIZE, "8250\n");
}

static DEVICE_ATTR (port, S_IRUGO | S_IWUSR, show_port, store_port);
static DEVICE_ATTR (irq, S_IRUGO | S_IWUSR, show_irq, store_irq);
static DEVICE_ATTR (base, S_IRUGO | S_IWUSR, show_base, store_base);
static DEVICE_ATTR (porttype, S_IRUGO | S_IWUSR, show_porttype, NULL);

struct eib_lowlevel
{
  int port;
  int irq;
  long base;
  spinlock_t irqlock;
  eib_handler_t handler;
  void *data;
  eib_config *conf;

  unsigned char old_dll;
  unsigned char old_dlh;
  unsigned char old_lcr;
  unsigned char old_mcr;
};

static void
eib_setRTS (eib_lowlevel * dev)
{
  outb (UART_MCR_DTR | UART_MCR_RTS | UART_MCR_OUT2, dev->port + UART_MCR);
}

static void
eib_clrRTS (eib_lowlevel * dev)
{
  outb (UART_MCR_DTR | UART_MCR_OUT2, dev->port + UART_MCR);
}

static int
eib_getCTS (eib_lowlevel * dev)
{
  return inb (dev->port + UART_MSR) & UART_MSR_CTS;
}

static void
eib_send (eib_lowlevel * dev, uint8_t val)
{
  outb (val, dev->port + UART_TX);
}

static int
eib_recv (eib_lowlevel * dev)
{
  return inb (dev->port + UART_RX);
}

static int
eib_overrun (eib_lowlevel * dev)
{
  return inb (dev->port + UART_LSR) & UART_LSR_OE;
}

static int
eib_send_finished (eib_lowlevel * dev)
{
  return (inb (dev->port + UART_LSR) & UART_LSR_TEMT);
}

static enum eib_reason
eib_getserialint (eib_lowlevel * dev)
{
  int id = inb (dev->port + UART_IIR);

  if (~id & UART_IIR_NO_INT)
    {
      if ((id & UART_IIR_ID) == UART_IIR_RDI)
	return EIB_INT_DATA;

      if ((id & UART_IIR_MSI) == UART_IIR_MSI)
	if (inb (dev->port + UART_MSR) & UART_MSR_DCTS)
	  return EIB_INT_MSR;

      return EIB_NONE;
    }
  return EIB_OTHER;
}


/*********************************************************************
 ** Name    : init_serial
 ** Purpose : No serial driver used/this module programs the UART
 **           itself (Speed: 9600Baud, 8 Databits, 1 Stoppbit)
 ********************************************************************/
static void
eib_init_serial (eib_lowlevel * dev)
{
  int divisor = dev->base / 9600;
  outb (0x00, dev->port + UART_IER);
  dev->old_lcr = inb (dev->port + UART_LCR);	/* save old line control reg */

  outb (UART_LCR_DLAB, dev->port + UART_LCR);	/* set divisor latch access bit in line control reg */
  dev->old_dll = inb (dev->port + UART_DLL);	/* save divisor latch low */
  dev->old_dlh = inb (dev->port + UART_DLM);	/* save divisor latch high 
						 * 115200 / Divisorlatch = speed
						 */
  outb (divisor & 0xff, dev->port + UART_DLL);	/* write 0x0c to divisor latch low */
  outb ((divisor >> 8) & 0xff, dev->port + UART_DLM);	/* write 0x00 to divisor latch high 
							 * Speed : 9600
							 */

  outb (UART_LCR_WLEN8, dev->port + UART_LCR);	/* 8 Databits in the line control reg */
  outb (0x00, dev->port + UART_FCR);	/* disable Fifo control reg */
  dev->old_mcr = inb (dev->port + UART_MCR);	/* save modem control reg */

  outb (UART_MCR_DTR | UART_MCR_OUT2, dev->port + UART_MCR);
  /* Set RTS, Enable GP02 neccessary for UART ints */
  outb (UART_IER_MSI | UART_IER_RDI, dev->port + UART_IER);
  /* Enable modem status int and receiver data int */

  eib_recv (dev);
}

/*********************************************************************
 ** Name    : restore_serial
 ** Purpose : Restores the settings of the UART control regs from the
 **           variables explained above
 ********************************************************************/
static void
eib_restore_serial (eib_lowlevel * dev)
{
  outb (0x00, dev->port + UART_IER);
  outb (UART_LCR_DLAB, dev->port + UART_LCR);	/* enable divisor latch access */
  outb (dev->old_dll, dev->port + UART_DLL);
  outb (dev->old_dlh, dev->port + UART_DLM);
  outb (dev->old_lcr, dev->port + UART_LCR);
  outb (dev->old_mcr, dev->port + UART_MCR);
}

static void
eib_free_serial (eib_lowlevel * dev)
{
  if (IS_ERR (dev))
    return;
  free_irq (dev->irq, dev);
  release_region (dev->port, 0x08);
  eib_port_free (&dev->conf->dev);
  kfree (dev);
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
static irqreturn_t
eib_interrupt_handler (int irq, void *dev_id, struct pt_regs *regs)
#else
static irqreturn_t
eib_interrupt_handler (int irq, void *dev_id)
#endif
{
  eib_lowlevel *dev = (eib_lowlevel *) dev_id;
  enum eib_reason reason;

  spin_lock (&dev->irqlock);

  reason = eib_getserialint (dev);
  if (reason != EIB_OTHER)
    dev->handler (dev->data, reason);
  spin_unlock (&dev->irqlock);

  return (reason == EIB_OTHER ? IRQ_NONE : IRQ_HANDLED);
}

static eib_lowlevel *
eib_create_serial (struct device *device, const char *name,
		   eib_handler_t handler, void *data)
{
  eib_lowlevel *dev;

  eib_config *d = to_eib_config (device);

  dev = (void *) kmalloc (sizeof (eib_lowlevel), GFP_KERNEL);
  if (!dev)
    return ERR_PTR (-ENOMEM);

  dev->port = d->port;
  dev->irq = d->irq;
  dev->base = d->base;
  dev->data = data;
  dev->handler = handler;
  dev->conf = d;
  spin_lock_init (&dev->irqlock);

  if (!request_region (dev->port, 0x08, name))
    goto outdev;

  if (request_irq
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
      (dev->irq, eib_interrupt_handler, SA_INTERRUPT | SA_SHIRQ, name,
#else
      (dev->irq, eib_interrupt_handler, IRQF_DISABLED | IRQF_SHARED, name,
#endif
       (void *) dev) < 0)
    {
      release_region (dev->port, 0x08);
      goto outdev;
    }

  return dev;

outdev:
  kfree (dev);
  return ERR_PTR (-ENODEV);
}

static eib_port eib_serial = {
  .setRTS = eib_setRTS,
  .clrRTS = eib_clrRTS,
  .getCTS = eib_getCTS,
  .send = eib_send,
  .recv = eib_recv,
  .open_port = eib_init_serial,
  .close_port = eib_restore_serial,
  .getserialint = eib_getserialint,
  .overrun = eib_overrun,
  .send_finished = eib_send_finished,
  .free_serial = eib_free_serial,
  .owner = THIS_MODULE,
};

static int
register_port (struct device *dev)
{
  int ret;

  ret = eib_port_register (dev, &eib_create_serial, &eib_serial);
  if (ret < 0)
    return ret;
  ret = device_create_file (dev, &dev_attr_port);
  if (ret < 0)
    goto unreg1;
  ret = device_create_file (dev, &dev_attr_irq);
  if (ret < 0)
    goto unreg2;
  ret = device_create_file (dev, &dev_attr_base);
  if (ret < 0)
    goto unreg3;
  ret = device_create_file (dev, &dev_attr_porttype);
  if (ret < 0)
    goto unreg4;
  return 0;

unreg4:
  device_remove_file (dev, &dev_attr_base);
unreg3:
  device_remove_file (dev, &dev_attr_irq);
unreg2:
  device_remove_file (dev, &dev_attr_port);
unreg1:
  eib_port_unregister (dev);
  return ret;
}

static void
deregister_port (struct device *dev)
{
  device_remove_file (dev, &dev_attr_porttype);
  device_remove_file (dev, &dev_attr_base);
  device_remove_file (dev, &dev_attr_irq);
  device_remove_file (dev, &dev_attr_port);
  eib_port_unregister (dev);
}

static int __init
eib_8250_init (void)
{
  int retval;
  int i;
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    {
      retval = register_port (&devs[i].dev);
      if (retval < 0)
	goto err;
    }
  return 0;
err:
  while (--i > 0)
    deregister_port (&devs[i].dev);
  return retval;
}

static void __exit
eib_8250_cleanup (void)
{
  int i;
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    deregister_port (&devs[i].dev);
}

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("Bernd Thallner, bernd@kangaroo.at");
MODULE_AUTHOR ("Martin K�gler <mkoegler@auto.tuwien.ac.at>");
MODULE_DESCRIPTION ("EIB port driver for 8250");

module_init (eib_8250_init);
module_exit (eib_8250_cleanup);
