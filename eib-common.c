/*
     EIB driver core

     Copyright (C) 2005-2008 Martin Koegler <mkoegler@auto.tuwien.ac.at>

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/poll.h>
#include <linux/fs.h>
#include <linux/err.h>
#include <linux/rwsem.h>

#include "eib-common.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,14)
#error This Linux version is no longer supported
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26)
#define HAVE_CLASS_DEVICE
#else
#undef HAVE_CLASS_DEVICE
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,15)
#else
#include <linux/platform_device.h>
#endif

#define MAX_DEVICE_COUNT 256

static struct platform_device *eib_device;
static struct device_driver eib_sysfs_driver = {
  .name = "eib",
  .bus = &platform_bus_type,
};

static struct class *eib_class;

typedef struct
{
  struct device *dev;
  eib_port *ops;
  int used;
  eib_create_serial_t create;
} eib_port_driver;

typedef struct _eib_driver
{
  struct _eib_driver *next;
  char name[15];
  char devicename[17];
  int major;
  eib_protocol_ops *ops;
} eib_driver;

static eib_port_driver devs[MAX_DEVICE_COUNT];
static eib_driver *eib_drivers = NULL;

static DECLARE_RWSEM (register_lock);

static void
eib_device_release (struct device *dev)
{
  return;
}

static void
eib_create_device (eib_driver * drv, int minor, struct device *dev)
{
  dev_t devid = MKDEV (drv->major, minor);
#ifdef HAVE_CLASS_DEVICE
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,15)
  class_device_create (eib_class, devid, dev, drv->devicename, minor);
#else
  class_device_create (eib_class, NULL, devid, dev, drv->devicename, minor);
#endif
#else
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27)
  device_create (eib_class, dev, devid, drv->devicename, minor);
#else
  device_create (eib_class, dev, devid, 0, drv->devicename, minor);
#endif
#endif
}

static void
eib_delete_device (eib_driver * drv, int minor)
{
  dev_t devid = MKDEV (drv->major, minor);
#ifdef HAVE_CLASS_DEVICE
  class_device_destroy (eib_class, devid);
#else
  device_destroy (eib_class, devid);
#endif
}

static void
eib_driver_add (eib_driver * drv)
{
  int i;
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    if (devs[i].dev)
      {
	eib_create_device (drv, i, devs[i].dev);
      }
}

static void
eib_driver_del (eib_driver * drv)
{
  int i;
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    if (devs[i].dev)
      {
	eib_delete_device (drv, i);
      }
}

static void
eib_port_add (int minor)
{
  eib_driver *drv;
  drv = eib_drivers;
  while (drv)
    {
      eib_create_device (drv, minor, devs[minor].dev);
      drv = drv->next;
    }
}

static void
eib_port_del (int minor)
{
  eib_driver *drv;
  drv = eib_drivers;
  while (drv)
    {
      eib_delete_device (drv, minor);
      drv = drv->next;
    }
}

int
eib_port_register (struct device *dev, eib_create_serial_t create,
		   eib_port * ops)
{
  int retval = -ENFILE;
  int i;
  down_write (&register_lock);
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    if (!devs[i].dev)
      {
	dev->parent = &eib_device->dev;
	dev->release = eib_device_release;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28)
	snprintf (dev->bus_id, sizeof (dev->bus_id), "eib%d", i);
#else
	dev_set_name (dev, "eib%d", i);
#endif
	
	retval = device_register (dev);
	if (!retval)
	  {
	    devs[i].ops = ops;
	    devs[i].dev = dev;
	    devs[i].create = create;
	    eib_port_add (i);
	  }
	break;
      }
  up_write (&register_lock);
  return retval;
}

void
eib_port_unregister (struct device *dev)
{
  int i;
  down_write (&register_lock);
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    if (devs[i].dev == dev)
      {
	BUG_ON (devs[i].used);
	eib_port_del (i);
	device_unregister (dev);
	devs[i].dev = NULL;
	devs[i].create = NULL;
	devs[i].ops = NULL;
	up_write (&register_lock);
	return;
      }
  BUG_ON (1);
  up_write (&register_lock);
}

eib_lowlevel *
eib_port_create (int minor, const char *name,
		 eib_handler_t handler, void *data, eib_port ** ops)
{
  eib_lowlevel *res;

  if (minor >= MAX_DEVICE_COUNT)
    return ERR_PTR (-ENODEV);

  down_read (&register_lock);

  if (!devs[minor].dev)
    {
      up_read (&register_lock);
      return ERR_PTR (-ENODEV);
    }

  if (devs[minor].used)
    {
      up_read (&register_lock);
      return ERR_PTR (-EBUSY);
    }

  if (!try_module_get (devs[minor].ops->owner))
    {
      up_read (&register_lock);
      return ERR_PTR (-ENODEV);
    }
  *ops = devs[minor].ops;

  res = (*devs[minor].create) (devs[minor].dev, name, handler, data);
  if (IS_ERR (res))
    module_put (devs[minor].ops->owner);
  else
    devs[minor].used = 1;

  up_read (&register_lock);
  return res;
}

void
eib_port_free (struct device *dev)
{
  int i;
  down_write (&register_lock);
  for (i = 0; i < MAX_DEVICE_COUNT; i++)
    if (devs[i].dev == dev)
      {
	BUG_ON (!devs[i].used);
	devs[i].used = 0;
	module_put (devs[i].ops->owner);
	up_write (&register_lock);
	return;
      }
  BUG_ON (1);
  up_write (&register_lock);
}

static int
eib_fasync (int fd, struct file *file, int mode)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;

  return fasync_helper (fd, file, mode, &cdev->async);
}

static ssize_t
eib_read (struct file *file, char __user * buf, size_t count, loff_t * offset)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  int rb_tail, len;

retry:
  while (cdev->read_next == cdev->read_free)
    {
      if (file->f_flags & O_NONBLOCK)	/* read buffer empty */
	return -EAGAIN;
      wait_event_interruptible (cdev->read_queue,
				!(cdev->read_next == cdev->read_free));

      if (signal_pending (current))
	return -ERESTARTSYS;
    }

  spin_lock (&cdev->read_lock);


  if (cdev->read_next == cdev->read_free)
    {
      spin_unlock (&cdev->read_lock);
      goto retry;
    }

  rb_tail = cdev->read_next;
  len = cdev->read_buffer[rb_tail][0];

  if (count < len)
    {
      spin_unlock (&cdev->read_lock);
      return -ENOMSG;
    }

  if (copy_to_user (buf, &cdev->read_buffer[rb_tail][1], len))
    {
      spin_unlock (&cdev->read_lock);
      return -EFAULT;
    }

  cdev->read_next = (cdev->read_next + 1) % EIB_MAX_READ_BUFFER;

  cdev->read_head = (cdev->read_head + 1) % EIB_MAX_READ_BUFFER;

  spin_unlock (&cdev->read_lock);

  return len;
}

static ssize_t
eib_write (struct file *file,
	   const char __user * buf, size_t count, loff_t * offset)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  int temp, wb_head;
  eib_buffer buffer;

  if (count >= sizeof (eib_buffer) - 1)
    return -EINVAL;

  if (copy_from_user (buffer + 1, buf, count))
    return -EFAULT;

retry:
  while ((cdev->write_free + 1) % EIB_MAX_WRITE_BUFFER == cdev->write_head)
    {
      /* write buffer full */

      if (file->f_flags & O_NONBLOCK)
	return -EAGAIN;
      wait_event_interruptible (cdev->write_queue,
				!((cdev->write_free +
				   1) % EIB_MAX_WRITE_BUFFER ==
				  cdev->write_head));

      if (signal_pending (current))
	return -ERESTARTSYS;
    }

  spin_lock (&cdev->write_lock);

  if ((cdev->write_free + 1) % EIB_MAX_WRITE_BUFFER == cdev->write_head)
    {
      spin_unlock (&cdev->write_lock);
      goto retry;
    }

  temp = cdev->ops->write_prepare (cdev->data, buffer, count);
  if (temp < 0)
    {
      spin_unlock (&cdev->write_lock);
      return temp;
    }

  wb_head = cdev->write_free;
  cdev->write_free = (cdev->write_free + 1) % EIB_MAX_WRITE_BUFFER;

  memcpy (cdev->write_buffer[wb_head], buffer, sizeof (buffer));
  cdev->write_result[wb_head] = 0;

  cdev->write_next = (cdev->write_next + 1) % EIB_MAX_WRITE_BUFFER;
  spin_unlock (&cdev->write_lock);

  cdev->ops->write_start (cdev->data);

  if (file->f_flags & O_NONBLOCK)
    return count;

  while (!cdev->write_result[wb_head])
    {
      /* write buffer full */

      wait_event_interruptible (cdev->write_queue,
				cdev->write_result[wb_head]);

      if (signal_pending (current))
	return -EINTR;
    }

  if (cdev->write_result[wb_head] > 0)
    return count;
  else
    return 0;
}

static unsigned int
eib_poll (struct file *file, poll_table * wait)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  unsigned int mask = 0;

  poll_wait (file, &cdev->read_queue, wait);
  poll_wait (file, &cdev->write_queue, wait);

  spin_lock (&cdev->read_lock);
  if (cdev->read_free != cdev->read_next)
    mask |= POLLIN | POLLRDNORM;
  spin_unlock (&cdev->read_lock);

  spin_lock (&cdev->write_lock);
  if ((cdev->write_free + 1) % EIB_MAX_WRITE_BUFFER != cdev->write_head)
    mask |= POLLOUT | POLLWRNORM;
  if (cdev->write_head == cdev->write_next)
    mask |= POLLPRI;
  spin_unlock (&cdev->write_lock);

  return mask;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
static int
eib_flush (struct file *file)
#else
static int
eib_flush (struct file *file, fl_owner_t id)
#endif
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  while (cdev->write_head != cdev->write_free)
    {				// write buffer not empty
      wait_event_interruptible (cdev->write_queue,
				!(cdev->write_head != cdev->write_free));
      if (signal_pending (current))
	return -ERESTARTSYS;
    }

  return 0;
}

static int
eib_fsync (struct file *file, struct dentry *d, int datasync)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  while (cdev->write_head != cdev->write_free)
    {				// write buffer not empty
      wait_event_interruptible (cdev->write_queue,
				!(cdev->write_head != cdev->write_free));
      if (signal_pending (current))
	return -ERESTARTSYS;
    }

  return 0;
}

static int
eib_ioctl (struct inode *inode,
	   struct file *file, unsigned int cmd, unsigned long arg)
{
  eib_chardev *cdev = (eib_chardev *) file->private_data;
  return cdev->ops->ioctl (cdev->data, cmd, arg);
}

static int
eib_open (struct inode *inode, struct file *file)
{
  eib_chardev *cdev;
  int retval;
  unsigned int minor = iminor (inode);
  unsigned int major = imajor (inode);
  eib_protocol_ops *ops;
  eib_driver *drv;

  down_read (&register_lock);
  drv = eib_drivers;
  while (drv)
    {
      if (major == drv->major)
	break;
      drv = drv->next;
    }
  if (!drv)
    {
      up_read (&register_lock);
      return -ENODEV;
    }
  ops = drv->ops;
  up_read (&register_lock);

  cdev = (void *) kmalloc (sizeof (eib_chardev), GFP_KERNEL);
  if (!cdev)
    return -ENOMEM;
  cdev->async = NULL;

  cdev->read_head = 0;
  cdev->read_free = 0;
  cdev->read_next = 0;
  spin_lock_init (&cdev->read_lock);
  init_waitqueue_head (&cdev->read_queue);

  cdev->write_head = 0;
  cdev->write_free = 0;
  cdev->write_next = 0;
  spin_lock_init (&cdev->write_lock);
  init_waitqueue_head (&cdev->write_queue);

  cdev->ops = ops;
  if (!try_module_get (cdev->ops->owner))
    {
      retval = -ENODEV;
      goto out;
    }
  file->private_data = cdev;
  cdev->data = cdev->ops->create (minor, cdev);
  if (IS_ERR (cdev->data))
    {
      retval = PTR_ERR (cdev->data);
      goto out;
    }

  return 0;
out:
  kfree (cdev);
  return retval;
}

static int
eib_release (struct inode *inode, struct file *file)
{
  eib_chardev *cdev = file->private_data;

  cdev->ops->shutdown (cdev->data);
  eib_fasync (-1, file, 0);

  flush_scheduled_work ();
  synchronize_rcu ();

  cdev->ops->free (cdev->data);
  module_put (cdev->ops->owner);

  kfree (cdev);

  return 0;
}

static struct file_operations eib_fops = {
  .owner = THIS_MODULE,
  .read = eib_read,
  .write = eib_write,
  .poll = eib_poll,
  .ioctl = eib_ioctl,
  .open = eib_open,
  .flush = eib_flush,
  .fsync = eib_fsync,
  .release = eib_release,
  .fasync = eib_fasync,
};

int
eib_driver_register (const char *name, int major, eib_protocol_ops * ops)
{
  int retval;
  eib_driver *drv;
  down_write (&register_lock);
  drv = eib_drivers;
  while (drv)
    {
      if (!strcmp (name, drv->name))
	{
	  up_write (&register_lock);
	  return -EEXIST;
	}
      drv = drv->next;
    }
  drv = kmalloc (sizeof (eib_driver), GFP_KERNEL);
  if (!drv)
    {
      up_write (&register_lock);
      return -ENOMEM;
    }
  strncpy (drv->name, name, sizeof (drv->name));
  drv->name[sizeof (drv->name) - 1] = 0;
  strcpy (drv->devicename, drv->name);
  strcat (drv->devicename, "%d");
  drv->ops = ops;
  retval = register_chrdev (major, drv->name, &eib_fops);
  if (drv->major < 0)
    {
      kfree (drv);
      up_write (&register_lock);
      return retval;
    }
  if (major == 0)
    drv->major = retval;
  else
    drv->major = major;
  drv->next = eib_drivers;
  eib_drivers = drv;
  eib_driver_add (drv);
  up_write (&register_lock);
  return 0;
}

void
eib_driver_unregister (const char *name)
{
  eib_driver *drv, **drv1;
  down_write (&register_lock);
  drv = eib_drivers;
  drv1 = &eib_drivers;
  while (drv)
    {
      if (!strcmp (name, drv->name))
	{
	  *drv1 = drv->next;
	  eib_driver_del (drv);
	  kfree (drv);
	  up_write (&register_lock);
	  return;
	}
      drv1 = &drv->next;
      drv = drv->next;
    }
  BUG_ON (1);
  up_write (&register_lock);
}


EXPORT_SYMBOL_GPL (eib_port_register);
EXPORT_SYMBOL_GPL (eib_port_unregister);
EXPORT_SYMBOL_GPL (eib_port_create);
EXPORT_SYMBOL_GPL (eib_port_free);
EXPORT_SYMBOL_GPL (eib_driver_register);
EXPORT_SYMBOL_GPL (eib_driver_unregister);

static int __init
eib_common_init (void)
{
  int retval;

  memset (devs, 0, sizeof (devs));

  retval = driver_register (&eib_sysfs_driver);
  if (retval != 0)
    goto unreg1;

  eib_device = platform_device_register_simple ("eib", -1, NULL, 0);
  if (IS_ERR (eib_device))
    {
      retval = PTR_ERR (eib_device);
      goto unreg2;
    }

  eib_class = class_create (THIS_MODULE, "eib");

  if (IS_ERR (eib_class))
    {
      retval = PTR_ERR (eib_class);
      goto unreg3;
    }

  printk ("EIB core loaded\n");
  return 0;

unreg3:
  platform_device_unregister (eib_device);

unreg2:
  driver_unregister (&eib_sysfs_driver);

unreg1:
  return retval;
}

static void __exit
eib_common_cleanup (void)
{
  class_destroy (eib_class);
  platform_device_unregister (eib_device);
  driver_unregister (&eib_sysfs_driver);
  printk ("EIB core unloaded\n");
}

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("Martin K�gler <mkoegler@auto.tuwien.ac.at>");
MODULE_DESCRIPTION ("EIB driver core");

module_init (eib_common_init);
module_exit (eib_common_cleanup);
