/*
     eib-pei16.c - Low level EIB driver for BCU 1

     Copyright (C) 2000 Bernd Thallner <bernd@kangaroo.at>
     Copyright (C) 2005-2008 Martin Koegler <mkoegler@auto.tuwien.ac.at>

     Ported to Linux 2.6 by Robert Kurevija
     Heavily modfied and restructed by Martin K�gler

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/sched.h>

#include "eib-common.h"
#include "eib.h"

#define EIB_DEBUG 0

#undef EIB_PRINT
#define EIB_PRINT(fmt, args...) printk(KERN_NOTICE "eib: " fmt "\n", ## args)

#undef PDEBUG
#if EIB_DEBUG
#  define PDEBUG(fmt, args...) printk(KERN_NOTICE "eib: " fmt "\n", ## args)
#else
# define PDEBUG(fmt, args...) do {} while (0)
#endif

#define EIB_LOG(MSG, args...) do { struct timespec tv; getnstimeofday (&tv); PDEBUG("at %010ld.%09ld " MSG,tv.tv_sec,tv.tv_nsec,##args);tv.tv_nsec--;} while(0)

static int eib_major = 0;

module_param (eib_major, int, 0);
MODULE_PARM_DESC (eib_major, "Major number for this device.");

MODULE_AUTHOR ("Martin K�gler, mkoegler@auto.tuwien.ac.at");
MODULE_AUTHOR ("Bernd Thallner, bernd@kangaroo.at");
MODULE_DESCRIPTION ("A low level EIB driver for BCU 1.");
MODULE_LICENSE ("GPL");

/* maximum time in usec to wait between rx interrupt raised and byte fully send */
#define MAX_TEMT_WAIT 1000

/* minimum time in usec to wait between byte fully send and clear rts signal */
#define WRITE_TEMT_CLRRTS_DELAY 100

/* minimum time in usec to wait between clear rts and set rts signal */
#define WRITE_CLRRTS_SETRTS_DELAY 80

/* timeout for data block transfer */
#define READ_WRITE_TIMEOUT 130000

/* maximum time between to interrupts if not in idle state */
#define MAX_TIME_BETWEEN_INT 80000

/* minimum time to wait after reset without any respond to signals */
#define MIN_RESET_WAIT_TIME 130000

/* maximum time between to wait for start sending */
#define MAX_INIT_SEND_TIMEOUT 100000

/* time to start transmission */
#define MAX_INIT_SEND_TIME 3000

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,26)

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,21)
#define HRTIMER_MODE_REL HRTIMER_REL
#endif

#include <linux/hrtimer.h>
#include <linux/ktime.h>

typedef struct eib_timer eib_timer;
typedef void (*eib_timer_func) (eib_timer * timer);

struct eib_timer
{
  struct hrtimer tl;
  eib_timer_func func;
};

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,21)
static int
eib_timer_run (struct hrtimer *data)
#else
static enum hrtimer_restart
eib_timer_run (struct hrtimer *data)
#endif
{
  eib_timer *t = container_of (data, eib_timer, tl);
  t->func (t);
  return HRTIMER_NORESTART;
}

static inline void
init_eib_timer (eib_timer * timer, eib_timer_func func)
{
  hrtimer_init (&timer->tl, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
  timer->func = func;
  timer->tl.function = eib_timer_run;
}

static inline void
start_eib_timer (eib_timer * timer, unsigned long usec_offset)
{
  if (!usec_offset)
    usec_offset = 1;
  hrtimer_start (&timer->tl, ktime_set (0, usec_offset * 1000),
		 HRTIMER_MODE_REL);
}

static inline void
stop_eib_timer (eib_timer * timer)
{
  hrtimer_try_to_cancel (&timer->tl);
}

static inline void
destroy_eib_timer (eib_timer * timer)
{
  hrtimer_cancel (&timer->tl);
}

#else

typedef struct eib_timer eib_timer;
typedef void (*eib_timer_func) (eib_timer * timer);

struct eib_timer
{
  struct timer_list tl;
  eib_timer_func func;
};

static void
eib_timer_run (unsigned long data)
{
  eib_timer *t = (eib_timer *) data;
  t->func (t);
}

static inline void
init_eib_timer (eib_timer * timer, eib_timer_func func)
{
  init_timer (&timer->tl);
  timer->func = func;
  timer->tl.function = eib_timer_run;
  timer->tl.data = (unsigned long int) (void *) timer;
}

static inline void
start_eib_timer (eib_timer * timer, unsigned long usec_offset)
{
  mod_timer (&timer->tl,
	     usec_offset ? jiffies +
	     usecs_to_jiffies (usec_offset) : jiffies + 1);
}

static inline void
stop_eib_timer (eib_timer * timer)
{
  del_timer (&timer->tl);
}

static inline void
destroy_eib_timer (eib_timer * timer)
{
  del_timer_sync (&timer->tl);
}

#endif

struct eib_protocol_data
{
  eib_chardev *cdev;
  int minor;
  spinlock_t lock;
  int shutdown;
  eib_port *iface;
  eib_lowlevel *dev;

  int read_pos;
  int write_pos;

  eib_timer initate_send_timer;
  eib_timer detect_reset_timer;
  eib_timer write_timer;

  struct timespec last_int_tv;
  struct timespec start_tv;
  struct timespec send_tv;
  struct timespec reset_tv;
  volatile int state;
  volatile int initate_send;
  int reset_count;
};

static int
bitcount (unsigned char x)
{
  int b;

  for (b = 0; x != 0; x >>= 1)
    if (x & 01)
      b++;

  return b;
}

static long
time_difference (struct timespec *tv1, struct timespec *tv2)
{
  struct timespec foo;

  if (tv2 == NULL)
    {
      getnstimeofday (&foo);
      tv2 = &foo;
    }

  return (tv2->tv_nsec - tv1->tv_nsec) / 1000 +	/* usecs   */
    1000000 * (tv2->tv_sec - tv1->tv_sec);	/* seconds * 1000000 */
}

static void
add_initate_send_timer (eib_protocol_data * dev)
{
  long t1, t2;
  if (dev->shutdown)
    {
      return;
    }
  t1 = MAX_INIT_SEND_TIME - time_difference (&dev->last_int_tv, NULL);
  t2 = MAX_INIT_SEND_TIMEOUT - time_difference (&dev->send_tv, NULL);
  EIB_LOG ("add_initate_send: %ld %ld", t1, t2);
  if (t1 < 0)
    t1 = 0;
  if (t2 < 0)
    t2 = 0;

  if (t2 < t1)
    t1 = t2;
  start_eib_timer (&dev->initate_send_timer, t1);
}

static void
add_detect_reset_timer (eib_protocol_data * dev)
{
  long t1;
  if (dev->shutdown)
    {
      return;
    }
  if (dev->state == 7)
    {
      t1 = MIN_RESET_WAIT_TIME - time_difference (&dev->reset_tv, NULL);
      EIB_LOG ("add_detect_reset: %ld", t1);
      if (t1 < 0)
	t1 = 0;
    }
  else
    {
      long t2;
      t1 = READ_WRITE_TIMEOUT - time_difference (&dev->start_tv, NULL);
      t2 = MAX_TIME_BETWEEN_INT - time_difference (&dev->last_int_tv, NULL);
      EIB_LOG ("add_detect_reset: %ld %ld", t1, t2);
      if (t1 < 0)
	t1 = 0;
      if (t2 < 0)
	t2 = 0;
      if (t2 < t1)
	t1 = t2;
    }

  start_eib_timer (&dev->detect_reset_timer, t1);
}

static void
add_write_timer (eib_protocol_data * dev, long timeout)
{
  if (dev->shutdown)
    {
      return;
    }
  EIB_LOG ("add_write_timer: %ld", timeout);
  start_eib_timer (&dev->write_timer, timeout);
}

static void
do_reset1 (eib_protocol_data * dev, unsigned long reason, const char *msg,
	   int line)
{

  EIB_PRINT
    ("do_reset: state=%d, reason=%lx, initate_send=%d, getCTS()=%d, %s at %d",
     dev->state, reason, dev->initate_send, dev->iface->getCTS (dev->dev),
     msg, line);

  dev->state = 7;
  dev->iface->getserialint (dev->dev);
  dev->iface->recv (dev->dev);
  dev->iface->getCTS (dev->dev);	/* clear MSR Delta */
  getnstimeofday (&dev->reset_tv);
  dev->iface->clrRTS (dev->dev);
}

#define do_reset(dev, MSG) do_reset1(dev, MSG , __LINE__)

#define ERR_1         0x00000001   , "no interrupt pending"
#define ERR_2         0x00000002   , "unexpected receive data interrupt"
#define ERR_4         0x00000004   , "unexpected receive data interrupt"
#define ERR_8         0x00000008   , "unexpected cts change to high"
#define ERR_10        0x00000010   , "read buffer overflow"
#define ERR_20        0x00000020   , "unexpected cts change to high"
#define ERR_40        0x00000040   , "unknown state"
#define ERR_80        0x00000080   , "overrun"
#define ERR_100       0x00000100   , "read forced"
#define ERR_200       0x00000200   , "unexpected receive data interrupt"
#define ERR_400       0x00000400   , "parity error"
#define ERR_800       0x00000800   , "unexpected receive data interrupt"
#define ERR_1000      0x00001000   , "exceed MAX_TEMT_WAIT"
#define ERR_2000      0x00002000   , "unexpected byte received"
#define ERR_4000      0x00004000   , "read forced && read buffer full"
#define ERR_8000      0x00008000   , "unexpected cts change"
#define ERR_10000     0x00010000   , "initate send with empty write buffer"
#define ERR_20000     0x00020000   , "bcu reset (info)"
#define ERR_40000     0x00040000   , "unexpected receive data interrupt"
#define ERR_80000     0x00080000   , "unexpected cts change to high"
#define ERR_100000    0x00100000   , "READ_WRITE_TIMEOUT"
#define ERR_200000    0x00200000   , "exceed MAX_TIME_BETWEEN_INT"
#define ERR_400000    0x00400000   , "clear write buffer"
#define ERR_800000    0x00800000   , "BCU reset"
#define ERR_10000000  0x10000000   , "INITIATESEND"

static void
check_interrupt_pending (eib_protocol_data * dev)
{
  switch (dev->iface->getserialint (dev->dev))
    {
    case EIB_INT_DATA:
      do_reset (dev, ERR_40000);	// unexpected receive data interrupt
      break;
    case EIB_INT_MSR:
      if (dev->iface->getCTS (dev->dev))
	do_reset (dev, ERR_80000);	// unexpected cts change to high
      break;
    default:
      break;
    }
}

static void
eib_handler (eib_protocol_data * dev, enum eib_reason reason)
{
  unsigned long flags;
  int tmp;

  EIB_LOG ("Handler %d %d %d", reason, dev->state,
	   dev->iface->getCTS (dev->dev));

  switch (dev->state)
    {				/* State machine state */
    case 0:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_2);	/* unexpected receive data interrupt */
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    {
	      if (dev->initate_send)
		{		/* write to bcu */
		  dev->iface->send (dev->dev, eib_writebuffer (dev->cdev)[0]);
		  dev->initate_send = 0;
		  dev->write_pos++;
		  dev->state = 5;

		  break;
		}
	      else
		{		// read
		  add_detect_reset_timer (dev);
		  if (eib_readbuffer_full (dev->cdev))
		    {
		      do_reset (dev, ERR_10);	/* read buffer overflow */
		    }
		  else
		    {
		      getnstimeofday (&dev->start_tv);
		      dev->iface->setRTS (dev->dev);
		      dev->iface->send (dev->dev, 0xFF);
		      dev->state = 2;
		    }
		}
	    }			// else !getCTS() -> state=0
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  do_reset (dev, ERR_40);	/* unknown state */
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 1:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_200);	// unexpected receive data interrupt
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    {
	      dev->iface->setRTS (dev->dev);
	      dev->iface->send (dev->dev, 0x00);
	      dev->state = 2;
	      add_detect_reset_timer (dev);
	    }			// else !getCTS() -> state=1
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  do_reset (dev, ERR_40);	/* unknown state */
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 2:
      switch (reason)
	{
	case EIB_INT_DATA:
	  tmp = dev->iface->recv (dev->dev);
	  eib_readbuffer (dev->cdev)[dev->read_pos++] = tmp;
	  if (dev->iface->overrun (dev->dev))
	    {
	      do_reset (dev, ERR_80);	// overrun
	      break;
	    }
	  if (tmp == 0xA0 && dev->read_pos == 1)
	    {
	      eib_readbuffer (dev->cdev)[0] = 1;
	      eib_readbuffer (dev->cdev)[1] = tmp;
	      eib_read_notify (dev->cdev);
	      do_reset (dev, ERR_800000);	// BCU reset
	      break;
	    }

	  if (tmp == 0xFF && dev->read_pos == 1)
	    {
	      EIB_PRINT ("No data");
	      dev->state = 0;
	      check_interrupt_pending (dev);
	      dev->iface->clrRTS (dev->dev);
	      dev->read_pos = 0;

	      getnstimeofday (&dev->last_int_tv);
	      break;
	    }

	  if (dev->read_pos == 1 && !(((tmp & 0x60) == 0x20) &&
				      ((tmp & 0x80) ==
				       (bitcount (tmp & 0x7f) % 2) * 0x80)))
	    {
	      do_reset (dev, ERR_400);	// parity error
	      break;
	    }

	  dev->state = 3;
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    do_reset (dev, ERR_20);	/* unexpected cts change to high */
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  do_reset (dev, ERR_40);	/* unknown state */
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 3:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_4);	/* unexpected receive data interrupt */
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    {
	      do_reset (dev, ERR_8);	/* unexpected cts change to high */
	      break;
	    }
	  if (dev->read_pos == (eib_readbuffer (dev->cdev)[0] & 0x1f) + 1)
	    {			// read finished

	      if (eib_readbuffer (dev->cdev)[0] == 0xa0)
		{
		  eib_readbuffer (dev->cdev)[0] = 1;
		  eib_readbuffer (dev->cdev)[0] = 0xa0;
		  eib_read_notify (dev->cdev);
		  EIB_PRINT ("BCU reset");
		}
	      else
		{
		  dev->reset_count = 0;
		  eib_readbuffer (dev->cdev)[0] &= 0x1f;
		  eib_read_notify (dev->cdev);
		}

	      dev->read_pos = 0;

	      spin_lock_irqsave (&dev->lock, flags);

	      if (!eib_writebuffer_empty (dev->cdev))
		{
		  getnstimeofday (&dev->send_tv);
		  add_initate_send_timer (dev);
		}
	      dev->state = 0;
	    }
	  else
	    {
	      spin_lock_irqsave (&dev->lock, flags);
	      dev->state = 1;
	    }
	  check_interrupt_pending (dev);
	  dev->iface->clrRTS (dev->dev);

	  spin_unlock_irqrestore (&dev->lock, flags);

	  getnstimeofday (&dev->last_int_tv);
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 4:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_800);	/* unexpected receive data interrupt */
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    {
	      dev->iface->send (dev->dev,
				eib_writebuffer (dev->cdev)[dev->
							    write_pos++]);
	      dev->state = 5;
	      add_detect_reset_timer (dev);
	    }			// else !getCTS() -> state=4
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  do_reset (dev, ERR_40);	/* unknown state */
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 5:
      switch (reason)
	{
	case EIB_INT_DATA:
	  tmp = dev->iface->recv (dev->dev);

	  dev->state = 6;

	  if (dev->iface->overrun (dev->dev))
	    {
	      do_reset (dev, ERR_80);	/* overun */
	      break;
	    }
	  if (dev->write_pos == 1 && tmp == 0xA0)
	    {
	      eib_readbuffer (dev->cdev)[1] = tmp;
	      eib_readbuffer (dev->cdev)[0] = 1;
	      eib_read_notify (dev->cdev);
	      do_reset (dev, ERR_800000);	// BCU reset
	      break;
	    }

	  if (dev->write_pos == 1 && tmp != 0xff)
	    {
	      dev->write_pos = 0;
	      if (eib_readbuffer_full (dev->cdev))
		{
		  do_reset (dev, ERR_4000);	/* read forced && read buffer full */

		  break;
		}

	      if (((tmp & 0x60) != 0x20) ||
		  ((tmp & 0x80) != (bitcount (tmp & 0x7f) % 2) * 0x80))
		{
		  do_reset (dev, ERR_400);	/* parity error */

		  break;
		}
	      eib_readbuffer (dev->cdev)[dev->read_pos++] = tmp;

	      dev->state = 3;
	      break;
	    }
	  if (dev->write_pos > 1 && tmp != 0x00)
	    {
	      do_reset (dev, ERR_2000);	/* unexpect byte received */
	      break;
	    }
	  add_write_timer (dev, MAX_TEMT_WAIT + WRITE_TEMT_CLRRTS_DELAY);
	  break;
	case EIB_INT_MSR:
	  do_reset (dev, ERR_8000);	/* unexpected cts change */
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  do_reset (dev, ERR_40);	/* unknown state */
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 6:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_4);	/* unexpected receive data interrupt */
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    do_reset (dev, ERR_8);	/* unexpected cts change to high */
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  if (!dev->iface->send_finished (dev->dev))
	    {
	      do_reset (dev, ERR_1000);	// exceed MAX_TEMT_WAIT
	      return;
	    }

	  if (dev->write_pos == (eib_writebuffer (dev->cdev)[0] & 0x1f) + 1)
	    {			// write finished
	      dev->reset_count = 0;
	      eib_write_finish (dev->cdev, 1);
	      dev->write_pos = 0;

	      if (!eib_writebuffer_empty (dev->cdev))
		{
		  getnstimeofday (&dev->send_tv);
		  add_initate_send_timer (dev);
		}

	      spin_lock_irqsave (&dev->lock, flags);


	      dev->state = 0;

	      check_interrupt_pending (dev);
	      dev->iface->clrRTS (dev->dev);
	      spin_unlock_irqrestore (&dev->lock, flags);
	    }
	  else
	    {
	      check_interrupt_pending (dev);

	      dev->iface->clrRTS (dev->dev);

	      spin_lock_irqsave (&dev->lock, flags);
	      dev->state = 8;
	      add_write_timer (dev, WRITE_CLRRTS_SETRTS_DELAY);
	      spin_unlock_irqrestore (&dev->lock, flags);
	    }

	  getnstimeofday (&dev->last_int_tv);
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 7:
      switch (reason)
	{
	case EIB_INT_DATA:
	  dev->iface->recv (dev->dev);
	  break;
	case EIB_INT_MSR:
	  dev->iface->getCTS (dev->dev);
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    case 8:
      switch (reason)
	{
	case EIB_INT_DATA:
	  do_reset (dev, ERR_4);	/* unexpected receive data interrupt */
	  break;
	case EIB_INT_MSR:
	  if (dev->iface->getCTS (dev->dev))
	    do_reset (dev, ERR_8);	/* unexpected cts change to high */
	  stop_eib_timer (&dev->write_timer);
	  goto rt2;
	  break;
	case EIB_TIME_RESET:
	  break;
	case EIB_TIME_SEND:
	  break;
	case EIB_TIME_WRITE:
	rt2:
	  spin_lock_irqsave (&dev->lock, flags);

	  dev->state = 4;

	  dev->iface->setRTS (dev->dev);
	  check_interrupt_pending (dev);
	  spin_unlock_irqrestore (&dev->lock, flags);

	  getnstimeofday (&dev->last_int_tv);
	  break;
	case EIB_OTHER:
	case EIB_NONE:
	  break;
	}
      break;
    default:
      do_reset (dev, ERR_40);	// unknown state    
    }
}

static void
write_timer (eib_timer * timer)
{
  eib_protocol_data *proto =
    container_of (timer, eib_protocol_data, write_timer);

  eib_handler (proto, EIB_TIME_WRITE);
}

static void
detect_reset_timer (eib_timer * timer)
{
  eib_protocol_data *dev =
    container_of (timer, eib_protocol_data, detect_reset_timer);
  unsigned long flags;

  spin_lock_irqsave (&dev->lock, flags);

  EIB_LOG ("detect_reset %d %d", dev->state, dev->iface->getCTS (dev->dev));

  if (dev->state == 0 && !dev->initate_send && !dev->iface->getCTS (dev->dev))
    {				// idle

      spin_unlock_irqrestore (&dev->lock, flags);

      return;
    }
  else if (dev->state == 7)
    {				// reset
      if (time_difference (&dev->reset_tv, NULL) > MIN_RESET_WAIT_TIME)
	{
	  getnstimeofday (&dev->last_int_tv);
	  if (!eib_writebuffer_empty (dev->cdev) && dev->reset_count > 10)
	    {
	      dev->reset_count = 0;
	      EIB_PRINT ("discard packet");
	      eib_write_finish (dev->cdev, -1);
	    }
	  if (!eib_writebuffer_empty (dev->cdev))
	    {
	      getnstimeofday (&dev->send_tv);
	      add_initate_send_timer (dev);
	    }
	  dev->read_pos = 0;
	  dev->write_pos = 0;
	  dev->state = 0;
	  dev->reset_count++;
	  spin_unlock_irqrestore (&dev->lock, flags);

	  return;
	}
    }
  else
    {				// read || write
      if (time_difference (&dev->start_tv, NULL) > READ_WRITE_TIMEOUT)
	do_reset (dev, ERR_100000);	// read write timeout
      else if (time_difference (&dev->last_int_tv, NULL) >
	       MAX_TIME_BETWEEN_INT)
	do_reset (dev, ERR_200000);	// max time between int exceeded
    }
  add_detect_reset_timer (dev);

  spin_unlock_irqrestore (&dev->lock, flags);
}

static void
initate_send_timer (eib_timer * timer)
{
  eib_protocol_data *dev =
    container_of (timer, eib_protocol_data, initate_send_timer);
  unsigned long flags;

  EIB_LOG ("Initiate_Send %d %d", dev->state, dev->iface->getCTS (dev->dev));

  if (eib_writebuffer_empty (dev->cdev))
    {
      EIB_PRINT ("initate send with empty buffer");
      return;
    }

  spin_lock_irqsave (&dev->lock, flags);

  if (dev->state == 0 && !dev->iface->getCTS (dev->dev) &&
      time_difference (&dev->last_int_tv, NULL) > MAX_INIT_SEND_TIME)
    {

      dev->initate_send = 1;
      getnstimeofday (&dev->start_tv);
      getnstimeofday (&dev->last_int_tv);

      dev->iface->setRTS (dev->dev);
      add_detect_reset_timer (dev);

      spin_unlock_irqrestore (&dev->lock, flags);

      return;
    }
  if (time_difference (&dev->send_tv, NULL) > MAX_INIT_SEND_TIMEOUT)
    {
      EIB_PRINT ("timeout initiate send");

      do_reset (dev, ERR_10000000);
      add_detect_reset_timer (dev);

      spin_unlock_irqrestore (&dev->lock, flags);

      return;
    }

  add_initate_send_timer (dev);

  spin_unlock_irqrestore (&dev->lock, flags);
}

static void
bcu_interrupt_handler (void *data, enum eib_reason reason)
{
  eib_protocol_data *dev = (eib_protocol_data *) data;

  eib_handler (dev, reason);

  if (dev->state != 0 && dev->state != 7 &&
      time_difference (&dev->start_tv, NULL) > READ_WRITE_TIMEOUT)
    do_reset (dev, ERR_100000);	/* read write timeout */

  getnstimeofday (&dev->last_int_tv);
}

static int
eib_write_prepare (eib_protocol_data * proto, eib_buffer buf, int len)
{
  int temp;

  if (len > 31)
    {
      return -EINVAL;
    }

  temp = (len & 0x1f) | 0x20;
  buf[0] = temp | (bitcount (temp) % 2) * 0x80;
  return 0;
}

static void
eib_write_start (eib_protocol_data * proto)
{
  unsigned long flags;
  spin_lock_irqsave (&proto->lock, flags);
  getnstimeofday (&proto->send_tv);
  add_initate_send_timer (proto);

  spin_unlock_irqrestore (&proto->lock, flags);
}

#define EIB_BCU_VERSION 1	/* eib bcu version */

static int
eib_proto_ioctl (eib_protocol_data * dev, unsigned int cmd, unsigned long arg)
{
  switch (cmd)
    {

    case EIB_GET_BCU_VERSION:
      return EIB_BCU_VERSION;

    case EIB_GET_DEVICE_INFORMATION:
      break;

    case EIB_RESET_DEVICE_STATISTIC:
      break;

    case EIB_CLEAR_WRITE_BUFFER:
      eib_writebuffer_clear (dev->cdev);

      stop_eib_timer (&dev->initate_send_timer);
      add_detect_reset_timer (dev);
      dev->initate_send = 0;
      do_reset (dev, ERR_400000);

      break;

    case EIB_CLEAR_READ_BUFFER:
      eib_readbuffer_clear (dev->cdev);

      stop_eib_timer (&dev->initate_send_timer);
      add_detect_reset_timer (dev);
      do_reset (dev, ERR_400000);

      break;

    case EIB_SINGLE_OPEN:
      break;

    default:
      return -ENOIOCTLCMD;
    }

  return 0;
}

static eib_protocol_data *
eib_proto_create (int minor, eib_chardev * cdev)
{
  eib_protocol_data *dev;
  unsigned long flags;
  int retval;

  EIB_PRINT ("open serial %d", minor);
  dev = (void *) kmalloc (sizeof (eib_protocol_data), GFP_KERNEL);
  if (!dev)
    return ERR_PTR (-ENOMEM);

  memset (dev, 0, sizeof (eib_protocol_data));
  dev->cdev = cdev;
  spin_lock_init (&dev->lock);
  dev->minor = minor;

  dev->dev =
    eib_port_create (minor, "eib", bcu_interrupt_handler, dev, &dev->iface);
  if (IS_ERR (dev->dev))
    {
      retval = PTR_ERR (dev->dev);
      goto outmem;
    }

  spin_lock_irqsave (&dev->lock, flags);
  init_eib_timer (&dev->initate_send_timer, initate_send_timer);
  init_eib_timer (&dev->detect_reset_timer, detect_reset_timer);
  init_eib_timer (&dev->write_timer, write_timer);

  dev->read_pos = 0;
  dev->write_pos = 0;
  dev->minor = minor;

  dev->state = 0;
  dev->initate_send = 0;
  dev->reset_count = 0;
  getnstimeofday (&dev->last_int_tv);

  dev->iface->open_port (dev->dev);
  spin_unlock_irqrestore (&dev->lock, flags);

  EIB_PRINT ("resouces allocated %d", dev->minor);
  return dev;

outmem:
  kfree (dev);
  return ERR_PTR (retval);
}

static void
eib_proto_shutdown (eib_protocol_data * dev)
{
  PDEBUG ("close %d", dev->minor);
  dev->shutdown = 1;
}

static void
eib_proto_free (eib_protocol_data * dev)
{
  unsigned long flags;
  spin_lock_irqsave (&dev->lock, flags);
  dev->iface->close_port (dev->dev);
  dev->iface->free_serial (dev->dev);

  destroy_eib_timer (&dev->initate_send_timer);
  destroy_eib_timer (&dev->write_timer);
  destroy_eib_timer (&dev->detect_reset_timer);

  spin_unlock_irqrestore (&dev->lock, flags);
  kfree (dev);
  PDEBUG ("closed %d", dev->minor);
}

static eib_protocol_ops eib_proto_ops = {
  .write_prepare = eib_write_prepare,
  .write_start = eib_write_start,
  .ioctl = eib_proto_ioctl,
  .shutdown = eib_proto_shutdown,
  .free = eib_proto_free,
  .create = eib_proto_create,
  .owner = THIS_MODULE,
};

static int __init
eib_pei16_init (void)
{
  int retval;

  retval = eib_driver_register ("eib", eib_major, &eib_proto_ops);
  if (retval < 0)
    return retval;

  EIB_PRINT ("pei16 driver loaded (%d)", eib_major);

  return 0;
}

static void __exit
eib_pei16_cleanup (void)
{
  eib_driver_unregister ("eib");
  EIB_PRINT ("pei16 driver successfully unloaded!\n");
}

module_init (eib_pei16_init);
module_exit (eib_pei16_cleanup);
